﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InboxService.Data;
using InboxService.Models;
using InboxService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InboxService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InboxController : ControllerBase
    {
        private readonly InboxItemData _inboxItemData;
        private readonly MessageItemData _messageItemData;


        public InboxController(MyDBContext context)
        {
            _inboxItemData = new InboxItemData(context);
            _messageItemData = new MessageItemData(context);
        }


        /// <summary>
        /// Get all conversation in the inbox
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(List<InboxItem>), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<List<InboxItem>> GetUsers()
        {
            //TODO: add pagination
            try
            {
                return Ok(_inboxItemData.findAll());
            }
            catch (Exception)
            {
                return BadRequest("bad input parameter");
            }
        }


        /// <summary>
        /// Get a particular conversation from the inbox
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(MessageItem), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 404)]
        public ActionResult<List<MessageItem>> GetMessageItemsByMatchId(string id)
        {
            if (!_inboxItemData.InboxItemExists(id))
                return NotFound("item does not exists");

            return Ok(_messageItemData.findBymatchId(id));
        }

        /// <summary>
        /// Create a new conversation in the inbox
        /// </summary>
        [HttpPost()]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<MessageItem> AddNewConversation([FromBody]MessageItem messageItem)
        {
            if (_inboxItemData.InboxItemExists(messageItem.match_id))
                return BadRequest("an existing item already exists");
            //add inbox item
            var res1 = _inboxItemData.addInboxItem(messageItem.match_id);
            if (res1 == null)
                return BadRequest("invalid input, object invalid");
            //add message item
            var res = _messageItemData.addMessageItem(messageItem,true);
            if (res == null )
                return BadRequest("invalid input, object invalid");
            return Created(res.id, res);
        }
        /// <summary>
        /// Add a new message in the conversation
        /// </summary>
        [HttpPost("id")]
        [ProducesResponseType(typeof(CreatedResult), 201)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        public ActionResult<MessageItem> AddMessageToConversation(string id, [FromBody] MessageItem messageItem)
        {
            if (id != messageItem.match_id)
                return BadRequest("invalid input, object invalid");
            if (!_inboxItemData.InboxItemExists(id))
                return BadRequest("item does not exists");
            var res = _messageItemData.addMessageItem(messageItem,false);
            if (res == null)
                return BadRequest("invalid input, object invalid");
            return Created(res.id, res);
        }


    }
}