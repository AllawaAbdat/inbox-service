﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InboxService.Migrations
{
    public partial class newMig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InboxItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    match_id = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InboxItems", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "MessageItems",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    from = table.Column<string>(nullable: false),
                    to = table.Column<string>(nullable: false),
                    match_id = table.Column<string>(nullable: false),
                    sent_date = table.Column<string>(nullable: false),
                    created_date = table.Column<string>(nullable: false),
                    content = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageItems", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InboxItems");

            migrationBuilder.DropTable(
                name: "MessageItems");
        }
    }
}
