﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InboxService.Data;
using InboxService.Models;

namespace InboxService.Services
{
    public class InboxItemData
    {
        private readonly MyDBContext _context;
        public InboxItemData(MyDBContext context)
        {
            _context = context;
        }

        public List<InboxItem> findAll()
        {
            var res = _context.InboxItems.ToList<InboxItem>();
            return res;
        }

        public InboxItem findById(string id)
        {
            return _context.InboxItems.Find(id);
        }

        public bool InboxItemExists(string match_id)
        {
            return _context.InboxItems.Any(e => e.match_id == match_id);
        }

        public InboxItem addInboxItem(string match_id)
        {
            try
            {
                InboxItem newInbox = new InboxItem
                {
                    match_id = match_id
                };
                _context.InboxItems.Add(newInbox);
                _context.SaveChanges();
                return newInbox;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
