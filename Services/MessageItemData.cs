﻿using InboxService.Data;
using InboxService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InboxService.Services
{
    public class MessageItemData
    {
        private readonly MyDBContext _context;
        public MessageItemData(MyDBContext context)
        {
            _context = context;
        }

        public List<MessageItem> findAll()
        {
            var res = _context.MessageItems.ToList();
            return res;
        }

        public List<MessageItem> findBymatchId(string match_id)
        {
            var res = _context.MessageItems
                .Where(m => m.match_id == match_id)
                .ToList<MessageItem>();
            if (res.Count > 0)
                return res;
            return null;
        }

        public MessageItem addMessageItem(MessageItem messageToAdd, bool newConv)
        {
            try
            {
                MessageItem newMessage;
                if (newConv)
                {
                    newMessage = new MessageItem
                    {
                        match_id = messageToAdd.match_id,
                        from = messageToAdd.from,
                        to = messageToAdd.to,
                        sent_date = DateTime.Now.ToString(),
                        created_date = DateTime.Today.ToString("dd/MM/yyyy"),
                        content = messageToAdd.content

                    };
                }
                else
                {
                    newMessage = new MessageItem
                    {
                        match_id = messageToAdd.match_id,
                        from = messageToAdd.from,
                        to = messageToAdd.to,
                        sent_date = DateTime.Now.ToString(),
                        created_date = messageToAdd.created_date,
                        content = messageToAdd.content

                    };
                }
                _context.MessageItems.Add(newMessage);
                _context.SaveChanges();
                return newMessage;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public MessageItem findById(string id)
        {
            return _context.MessageItems.Find(id);
        }
    }
}
