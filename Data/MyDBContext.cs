﻿using InboxService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InboxService.Data
{
    public class MyDBContext : DbContext
    {
        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options)
        {
        }

        public DbSet<InboxItem> InboxItems { get; set; }
        public DbSet<MessageItem> MessageItems { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //Auto - Incremented Id
            builder.Entity<InboxItem>()
                .Property(s => s.id)
                .ValueGeneratedOnAdd();

            builder.Entity<MessageItem>()
              .Property(s => s.id)
              .ValueGeneratedOnAdd();
        }

    }
}
