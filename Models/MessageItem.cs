﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InboxService.Models
{
    public class MessageItem
    {
        public string id { get; set; }
        [Required]
        public string from { get; set; }
        [Required]
        public string to { get; set; }
        [Required]
        public string match_id { get; set; }
        public string sent_date { get; set; }
        public string created_date { get; set; }
        [Required]
        public string content { get; set; }
    }
}
