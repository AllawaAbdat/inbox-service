﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InboxService.Models
{
    public class InboxItem
    {
        public string id { get; set; }
        [Required]
        public string match_id { get; set; }
    }
}
