Hi everyone !

On this repository you will be able to find a Docker image which allows you to run the Inbox Service for the CLO5 Project.

The tree of this Service is the following :

Dockerfile => File used to build a .NET docker image with the "Inbox-Service" API
Folder "CI" => Folder with 2 scripts (inbox_service_run.sh & inbox_service_deploy.sh). Used for the .gitlab-ci.yml
.gitlab-ci.yml => File which allows the Pipeline to be run on Gitlab. (With differents Jobs)

The following list is the User-Service API files :
-Controllers /
-Data /
-Migrations / 
-Models / 
-Services / 
-Migrations / 
-Properties / 
-bin / 
-Program.cs / 
-Startup.cs / 
-InboxService.csproj / 
-InboxService.csproj.user / 
-InboxService.sln
