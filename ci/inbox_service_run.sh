#!/bin/sh

set -e

  docker build -t my-inbox-service .
  docker run -d --name my-running-inbox-service my-inbox-service
  if docker ps | grep -q my-running-inbox-service; then
        echo Docker my-running-inbox-service found
    else
        echo Docker my-running-inbox-service not found
        exit
  fi